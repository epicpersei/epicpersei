


function setIntaFeed() {
	var instafotoSize = $('.instafoto').width();
	var feedHeight = (instafotoSize * 2) -20; 
	$('.instafoto').height(instafotoSize);

	$('.instaGramFeed').height(feedHeight);
}

function resolutionLineHeight() {
    var windowHeight = $(window).height();
    $('#resolution').css('line-height', windowHeight+ 'px');
}

function Site(){
	this.checkZones= function() {
		var windowHeight = $(window).height();
		zoneHeight = windowHeight-80;


		$('#portada').css('height', zoneHeight+'px')
	}
}

var site = new Site();


$(document).ready(function() {
	site.checkZones();
	setIntaFeed();
    resolutionLineHeight();
})

$(window).resize(function(){
	site.checkZones();
	setIntaFeed();
    resolutionLineHeight();
})




function openUrl(urlValue) {
	window.open(urlValue)
}


function toSection(valId) {
	$("html, body").animate({ scrollTop: $('#'+valId).offset().top }, 1000);
}



var facilitiesScripts = function(){

    var activeID = $(location).attr('hash').substring(1);
    if (activeID.length != "")
    {
        $('.facility').addClass('inactive');
        $('#'+activeID).removeClass('inactive');
        $('#'+activeID).addClass('active');
    }
    
    
    $('.stretchMe').each(function( index ) {
        $(this).backstretch($(this).attr('rel'));
    });
    
    $('.facility_img').click(function(){
        if($(this).parent().hasClass('inactive') || $(this).parent().hasClass('no_content'))
        {}
        else {
            $('.facility').addClass('inactive');
            $(this).parent().removeClass('inactive');
            $(this).parent().addClass('active');
            if($(this).parent().hasClass('slideUp'))
            {
                $('html, body').animate({scrollTop: $(this).parent().offset().top - 400}, 500, 'swing');
            }
            else {
                $('html, body').animate({scrollTop: $(this).parent().offset().top - 0}, 500, 'swing');
            }
        }
    });
    
    $('.facility_close').click(function(){
        $('.facility').removeClass('active');
        $('.facility').removeClass('inactive');
    });
};

$(window).load(function(){
    facilitiesScripts();    
});





























$(document).ready(function() {
	// Test for placeholder support
	$.support.placeholder = (function(){
		var i = document.createElement('input');
		return 'placeholder' in i;
	})();

    // Hide labels by default if placeholders are supported
    if($.support.placeholder) {
    	$('.form-label').each(function(){
    		$(this).addClass('js-hide-label');
    	});  

        // Code for adding/removing classes here
        $('.form-group').find('input, textarea').on('keyup blur focus', function(e){

            // Cache our selectors
            var $this = $(this),
            $parent = $this.parent().find("label");

            if (e.type == 'keyup') {
            	if( $this.val() == '' ) {
            		$parent.addClass('js-hide-label'); 
            	} else {
            		$parent.removeClass('js-hide-label');   
            	}                     
            } 
            else if (e.type == 'blur') {
            	if( $this.val() == '' ) {
            		$parent.addClass('js-hide-label');
            	} 
            	else {
            		$parent.removeClass('js-hide-label').addClass('js-unhighlight-label');
            	}
            } 
            else if (e.type == 'focus') {
            	if( $this.val() !== '' ) {
            		$parent.removeClass('js-unhighlight-label');
            	}
            }
        });
    } 
});